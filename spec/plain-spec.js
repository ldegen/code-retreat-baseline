import fancy from '../src/fancy';
import plain from '../src/plain';
describe('A plain javascript test case', ()=>{
  it("can import a TS unit", ()=>{
    expect(fancy(6,7)).to.equal(42);
  });
  it("can import a plain JS unit", ()=>{
    expect(plain(6,7)).to.equal(42);
  });
});
