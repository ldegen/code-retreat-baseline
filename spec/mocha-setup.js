import asPromised from 'chai-as-promised';
import almost from 'chai-almost';
import chai from 'chai';
import Promise from 'bluebird';
import "chai/register-expect.js";

chai.config.includeStack = false;
chai.use(asPromised);
chai.use(almost());

//global.expect = chai.expect;
//global.AssertionError = chai.AssertionError;
//global.Assertion = chai.Assertion;
//global.assert = chai.assert;
global.Promise = Promise;


