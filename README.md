# code-retreat-baseline

## Try

- Clone the repo, ``cd`` into it.
- Run ``npm ci`` once to make sure all dependencies are up to date.
- Run ``npm test`` to execute unit tests using mocha.
- Run ``npm run watch`` to keep mocha running and automatically rerun the tests
  when you change the code.

## Directory Structure

- Put your implementation `src`. 

- Put your unit tests into `spec`. Use filenames like ``{module name}-spec.ts``
  or ``{module name}-spec.js``.

- You can use typescript or plain javascript. Use ES Modules. Common-JS *may* work,
  but I have not tried it.

- In your imports, ommit the file extension.


